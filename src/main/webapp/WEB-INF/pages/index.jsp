<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype>
<html>
<head>
    <title>Home</title>
</head>
<body>
<div>
    <h1>Welcome!!!!</h1>

    <sec:authorize access="isAuthenticated()">
        <p>Hello, ${userDetails.username}!</p>
        <p>User authorities: ${userAuthorities}</p>
         <p>
		    <table>
				<c:forEach items="${applicableFields}" var="element"> 
				  <tr>
				    <td>${element.fieldName}</td>
				  </tr>
				</c:forEach>
			</table>
    </p>
        <p><a href="security/j_spring_security_logout">Sign Out</a></p>
    </sec:authorize>

   
<!--     <h2>Only Admins will be able to see this page.</h2> -->
<!--     <a href="admin.htm">Admin page</a> -->
<!--     </p> -->
</div>
</body>
</html>
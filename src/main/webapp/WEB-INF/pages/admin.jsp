<%@ page contentType="text/html;charset=UTF-8" language="java" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
    </head>
    <body>
        <h1>Admin-Only Page</h1>
        <p>Hello, ${userDetails.username}!</p>

        <p>User authorities: ${userAuthorities}</p>
		<table>
		<c:forEach items="${applicableFields}" var="element"> 
		  <tr>
		    <td>${element.fieldName}</td>
		  </tr>
		</c:forEach>
		</table>
        <a href="index.htm">Back</a>
    </body>
</html>
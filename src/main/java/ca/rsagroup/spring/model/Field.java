package ca.rsagroup.spring.model;

import java.io.Serializable;

public class Field implements Serializable{

	private static final long serialVersionUID = 8996454768983050877L;

	private int fieldId;
	
	private String fieldName;

	public int getFieldId() {
		return fieldId;
	}

	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	
	
}

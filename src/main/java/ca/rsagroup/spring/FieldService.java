package ca.rsagroup.spring;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ca.rsagroup.spring.model.Field;

@Service
public class FieldService {
	
	private static final Logger log = Logger.getLogger(FieldService.class);
	
	 protected DataSource ds;
	 protected NamedParameterJdbcTemplate jdbcTemplate;
	
    @Autowired
    public void setDataSource(DataSource ds){
        this.ds = ds;
        this.jdbcTemplate = new NamedParameterJdbcTemplate(ds);
    }
	
	public List<Field> getApplicableFields(UserDetails userDetails){
		return getFields(userDetails.getAuthorities().iterator().next().getAuthority());
	}
	
	
	@Transactional(readOnly = true)
    protected List<Field> getFields(String roleName) {
        String sql = "SELECT f.field_id, f.field_name" + 
					    " FROM fields_role_association frs" + 
						" JOIN roles r on frs.ROLE_ID = r.id"+
						" JOIN fields f on frs.FIELD_ID = f.FIELD_ID" + 
						" WHERE UPPER(r.name) = :role_name";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("role_name", roleName);
        List<Field> list = (List<Field>)this.jdbcTemplate.query(sql, params, new FieldRowMapper());
        return list;
    }

    private class FieldRowMapper implements RowMapper<ca.rsagroup.spring.model.Field>{
        @Override
        public Field mapRow(ResultSet rs, int rowNum) throws SQLException {
            Field field = new ca.rsagroup.spring.model.Field();
            field.setFieldName(StringUtils.trim(rs.getString("f.field_name")));
            field.setFieldId(rs.getInt("f.field_id"));
            return field;
        }

    }

}
